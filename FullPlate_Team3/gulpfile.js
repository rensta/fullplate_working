﻿var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var reactify = require('reactify');

gulp.task('build', function () {
    var bundleStream = browserify({
        entries: './scripts/src/main.jsx',
        transform: [reactify],
        debug: true
    }).bundle();

    bundleStream
        .pipe(source('main.js'))
        .pipe(gulp.dest('./scripts/dist'));
});

gulp.task('build:watch', function () {
    gulp.watch('./scripts/src/**/*.jsx', ['build']);
});