﻿using System.Web.Mvc;

namespace FullPlate_Team3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}