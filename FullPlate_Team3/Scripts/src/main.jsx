﻿
//Data for menu items
var data = [
'Late Requests',
'Orders',
'Totals',
'Restaurants'
];
//Random Data2 for page!
var data2 = [
<h1>This is static data to see if page is working correctly or not </h1>,
<h2><a href="https://www.devbridge.com/">DevBridge</a></h2>,

<table>
    <tr>
        <td>X</td>
        <td>Y</td>
    </tr>
    <tr>
        <td>To test?</td>
        <td>OR Not to test?</td>
    </tr>
</table>
];

//PageHeader - generates main page header :D
var PageHeader = React.createClass({
    render: function() {
        return (
          <header className="page-header">
              <div className="page-frame">
                  <p className="user-greeting">
                      <span>Hello</span>
                      <span className="user-name">Gediminas</span>
                      <span>
                          , 
                          <a href="/login/">logout</a>
                      </span>
                  </p>
                  <a href="/" className="logo">
                      <img src="/Content/images/fullplage-logo-white.svg" alt="logo" />
                  </a>
              </div>
          </header>
        );
    }
});

//MENU LIST - Generates a list of menu items
var Menu = React.createClass({
    render: function() {
        var menuNodes = this.props.data.map(function (menuitem) {
            return (
              <a href="/" className="nav-item">{menuitem}</a>
            );
        });
        return (
            <div className="page-sub-header">
                <div className="page-frame">
                  <div className="site-navigation extended">
                      {menuNodes}
                  </div>
                </div>
            </div>
        );
    }
});

// PageBody - Returns page body/data/information or anything else
var PageBody = React.createClass({
    render: function() {
        return (
          <div className="page-frame">
              {this.props.data}
          </div>
        );
    }
});

//Full page - Generates page header, menu layout, and rest of the page
var FullPage = React.createClass({
    render: function () {
        return (
        <div>
            <PageHeader />
            <Menu data={data}/>
            <PageBody data={data2} />
         </div>

      );
    }
});


//Renders page info
React.render(
    <FullPage />,
    document.getElementById('main')
);